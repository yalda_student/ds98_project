package Project;

import java.util.ArrayList;

public class Agency {

    private String name;
    private ArrayList<Service> availableServicesToOffer = new ArrayList<>();
    private MaxHeap maxHeapOfOrders;

    public Agency(String name) {
        this.name = name;
        maxHeapOfOrders = new MaxHeap();
    }

    @Override
    public String toString() {
        return name;
    }

    public ArrayList<Service> getAvailableServicesToOffer() {
        return availableServicesToOffer;
    }

    public void addServiceToAgency(Service service) {
        availableServicesToOffer.add(service);
    }

    public MaxHeap getMaxHeapOfOrders() {
        return maxHeapOfOrders;
    }
}
