package Project;

import java.util.ArrayList;
import java.util.Arrays;

public class MaxHeap {

    private int maxSize, n;
    private Order[] elements;

    public MaxHeap() {

        this.maxSize = 5;
        elements = new Order[maxSize + 1];
        n = 0;
    }

    private boolean isFull() {
        return n == maxSize - 1;
    }

    private boolean isEmpty() {
        return n == 0;
    }

    public boolean insert(Order value) {

        boolean flag = true;

        if (isFull())
            return false;

        n++;
        elements[n] = value;
        int i = n;

        while (i > 1 && flag) {
            if (elements[i].compareTo(elements[i / 2]) == 1)
                swap(i, i / 2);
            else flag = false;
            i = i / 2;
        }

        maxSize++;
        elements = Arrays.copyOf(elements, maxSize);
        return true;
    }

    private void swap(int i, int j) {

        Order temp = elements[i];
        elements[i] = elements[j];
        elements[j] = temp;
    }

    public boolean delete() {

        if (isEmpty())
            return false;

        boolean flag = true;
        elements[1] = elements[n];
        n--;
        int i = 1;

        while (flag && i <= n / 2) {
            int maxInt = 2 * i;
            if ((maxInt + 1) <= n && elements[maxInt + 1].compareTo(elements[maxInt]) == 1)
                maxInt = maxInt + 1;
            if (elements[i].compareTo(elements[maxInt]) == 1)
                swap(i, maxInt);
            else flag = false;
            i = maxInt;
        }
        return true;
    }

    public Order getMax() {
        return elements[1];
    }

    public void clear(Agency agency, ExtendedLinkList<Service> servicesList) {

        ArrayList<Service> services = agency.getAvailableServicesToOffer();

        for (int i = n; i > 1; i--) {

            Order order = this.getMax();
            String serviceName = order.getOrderServiceName();

            Service service = servicesList.searchNodeService(serviceName, servicesList.getFirst()).data;

            services.remove(service);

            System.out.println(order.toString());
            this.delete();
        }
        System.out.println(getMax());
    }
}
