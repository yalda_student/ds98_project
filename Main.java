package Project;

import java.sql.Timestamp;
import java.util.Scanner;

// 910 lines

public class Main {

    public static void main(String[] args) throws NotInBound {

        Scanner scanner = new Scanner(System.in);
        carsSystem carsSystem = new carsSystem();

        String name;
        System.out.println("Enter your name:");
        name = scanner.next();
        System.out.println("Hello " + name + " :)");
        String[] commands;
        String inputCommand, method;
        String flag = "Start";

        System.out.println("\nWhat's your first command " + name + "?");
        inputCommand = new Scanner(System.in).nextLine();

        while (!flag.equals("exit")) {

            commands = inputCommand.split("\\s");
            method = commands[0].toLowerCase();

            switch (method) {

                case "addservice":
                    Service service = new Service(commands[1]);
                    getInformation(service);
                    carsSystem.addService(service);
                    break;

                case "addsubservice":
                    Node<Service> node = carsSystem.getServicesList().searchNodeService(commands[3], carsSystem.getServicesList().getFirst());
                    if (node != null) {
                        Service subService = new Service(commands[1]);
                        getInformation(subService);
                        node.data.addSubService(subService);

                    } else {
                        System.out.println("This Service doesn't exist :(");
                    }

                case "addagency":
                    Agency agency = new Agency(commands[1]);
                    carsSystem.addAgency(agency);
                    break;

                case "addoffer":
                    String serviceName = commands[1], agencyName = commands[3];
                    carsSystem.addOffer(serviceName, agencyName);
                    break;

                case "agencieslist":
                    carsSystem.agencyList();
                    break;

                case "serviceslist":
                    carsSystem.servicesList();
                    break;

                case "listservicesfrom":
                    serviceName = commands[1];
                    carsSystem.subServicesFrom(serviceName);
                    break;

                case "order":
                    carsSystem.order(commands[1], commands[3], commands[5], Integer.parseInt(commands[7]), new Timestamp(System.currentTimeMillis()));
                    break;

                case "delete":
                    carsSystem.delete(commands[1], commands[3]);
                    break;

                case "listorders":
                    carsSystem.listOrders(commands[1]);
                    break;


                case "exit":
                    flag = "exit";
                    System.out.println("Hope to see you soon :)");
                    System.exit(0);
                    break;

            }
            System.out.println("\nWhat's your next command " + name + "?");
            inputCommand = new Scanner(System.in).nextLine();
        }
    }

    private static void getInformation(Service service) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("\nEnter model of your car: ");
        service.setCarModel(scanner.next());

        System.out.println("If you have some explanation for customer, write it: ");
        service.setExplanationForCustomer(new Scanner(System.in).nextLine());

        System.out.println("Here you can explain some technical points to agency: ");
        service.setTechnicalExplanationForAgency(new Scanner(System.in).nextLine());

        System.out.println("How much does this service cost?");
        String cost = scanner.next();

        boolean checker = true;

        while (checker) {
            try {
                service.setExpenses(Integer.parseInt(cost));
                checker = false;

            } catch (NumberFormatException nfe) {
                System.out.println("You must enter number!");
                System.out.println("How much does this service cost?");
                cost = scanner.next();
            }
        }
    }
}
