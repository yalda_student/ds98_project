package Project;

public class NotInBound extends Exception {

    String message;

    public NotInBound(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return message;
    }
}
