package Project;

public class Service {

    private String serviceName, carModel;
    private String ExplanationForCustomer, technicalExplanationForAgency;
    private int expenses;
    private int userCounter = 0, numberOfSubServices = 0;

    private Node<Service> subServices = new Node<>();
    private Node<Service> tail;

    public Service(String serviceName) {

        this.serviceName = serviceName;
    }

    public void addSubService(Service service) {

        Node<Service> node = new Node();
        node.data = service;
        node.link = null;

        if (numberOfSubServices == 0) {
            node.subService = null;
            numberOfSubServices++;

            subServices = node;
            tail = node;
            System.out.println("Added :)");
            return;

        } else {
            tail.link = node;
            tail = node;
            numberOfSubServices++;
            System.out.println("Added :)");
            return;
        }

    }

    public String getServiceName() {
        return serviceName;
    }

    public void showSubServices() {

        Node<Service> subNode = subServices;
        int counter = 1;

        if (subNode == null)
            return;

        while (subNode != null && counter <= numberOfSubServices) {

            System.out.println("subService's parent = " + this.serviceName);
            System.out.println(subNode.data.toString() + "\n");
            subNode.data.showSubServices();
            subNode = subNode.link;
            counter++;
        }
    }

    @Override
    public String toString() {
        return "Service name: " + serviceName + "\nCost: " + expenses
                + "\nExplanation For Customer: "
                + ExplanationForCustomer + "\nTechnical Explanation For Agency: " + technicalExplanationForAgency;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public void setExplanationForCustomer(String explanationForCustomer) {
        ExplanationForCustomer = explanationForCustomer;
    }

    public void setTechnicalExplanationForAgency(String technicalExplanationForAgency) {
        this.technicalExplanationForAgency = technicalExplanationForAgency;
    }

    public void setExpenses(int expenses) {
        this.expenses = expenses;
    }

    public void addUserCounter() {
        userCounter++;
    }

    public void decUserCounter() { userCounter--;}

    public void setSubServices(Node<Service> subServices) {
        this.subServices = subServices;
    }

    public int getUserCounter() {
        return userCounter;
    }

    public Node<Service> getSubServices() {

        return subServices;
    }

    public int getNumberOfSubServices() {
        return numberOfSubServices;
    }
}
