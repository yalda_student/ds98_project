package Project;

import java.sql.Timestamp;

public class carsSystem {

    public ExtendedLinkList<Service> servicesList = new ExtendedLinkList<>();
    public LinkedList agencies = new LinkedList();

    public boolean addService(Service service) {

        return servicesList.addService(service);
    }

    public boolean addOffer(String serviceName, String agencyName) throws NotInBound {

        int agencyIndex = searchAgency(agencyName, agencies.getFirst());

        Node<Service> serviceNode = servicesList.searchNodeService(serviceName, servicesList.getFirst());

        if (serviceNode == null) {
            System.out.println("We don't have this service :(");
            return false;
        }
        if (agencyIndex == -1) {
            System.out.println("We don't have this agency :(");
            return false;
        } else {
            Service currentService = serviceNode.data;
            Agency currentAgency = agencies.get(agencyIndex);
            currentService.addUserCounter();

            currentAgency.addServiceToAgency(currentService);
            System.out.println("Offered :)");
            return true;
        }
    }

    private int searchAgency(String agencyName, LinkedList.Node node) {

        int index = -1, i = -1;
        boolean flag = true;

        while (node != null && flag) {
            i++;
            if ((node.data).toString().equals(agencyName)) {
                index = i;
                flag = false;
            } else if (node.next != null)
                searchAgency(agencyName, node.next);
        }

        return index;
    }

    public void addAgency(Agency agency) {

        agencies.Insert(agency, agencies.getLength());
        System.out.println("Agency added :)");
    }

    /***
     *to see list of agencies
     */
    public void agencyList() throws NotInBound {

        for (int i = 0; i < agencies.getLength(); i++)
            System.out.println(agencies.get(i).toString());
    }

    /**
     * to see services and sub services
     */
    public void servicesList() {

        if (servicesList.getLength()<0)
            System.out.println("We don't have any service !");
        else {
            for (int i = 0; i < servicesList.getLength(); i++) {

                System.out.println(servicesList.getService(i).toString() + "\n\n");
                if (servicesList.getService(i).getNumberOfSubServices() != 0)
                    servicesList.getService(i).showSubServices();
            }
        }
    }

    /**
     * to see sub services from a service
     *
     * @param service
     */
    public void subServicesFrom(String service) {

        Node<Service> serviceNode = servicesList.searchNodeService(service, servicesList.getFirst());

        if (serviceNode == null)
            System.out.println("We don't have this service.");

        else {
            Service service1 = serviceNode.data;
            Node node = service1.getSubServices();

            while (node != null) {

                System.out.println(node.data.toString() + "\n");
                node = node.link;
            }
        }
    }

    public boolean order(String serviceName, String agencyName, String customerName, int immediacyLevel, Timestamp arrivalTime) throws NotInBound {

        int agencyInd = searchAgency(agencyName, agencies.getFirst());
        Node<Service> serviceNode = servicesList.searchNodeService(serviceName, servicesList.getFirst());

        if (agencyInd == -1) {
            System.out.println("We don't have this agency :(");
            return false;

        } else if (serviceNode == null) {
            System.out.println("We don't have this service :(");
            return false;

        } else {
            if (agencies.get(agencyInd).getAvailableServicesToOffer().contains(serviceNode.data)) {

                MaxHeap maxHeap = agencies.get(agencyInd).getMaxHeapOfOrders();
                Order order = new Order(serviceName, agencyName, customerName, immediacyLevel, arrivalTime);
//                System.out.println("time = " + order.getArrivalTime());
                maxHeap.insert(order);
                System.out.println("Ordered :)");
                return true;
            } else {
                System.out.println("**ATTENTION\nThis agency doesn't offer this service.\nYou can offer this service to agency, then order it.");
            }
            return false;
        }
    }

    public void delete(String serviceName, String agencyName) throws NotInBound {

        Node<Service> nodeService = servicesList.searchNodeService(serviceName, servicesList.getFirst());
        int agencyIndex = searchAgency(agencyName, agencies.getFirst());

        if (agencyIndex == -1) {
            System.out.println("We don't have this agency :(");
            return;

        } else if (nodeService == null) {
            System.out.println("We don't have this service :(");
            return;

        } else {

            Service service = nodeService.data;
            Agency agency = agencies.get(agencyIndex);

            agency.getAvailableServicesToOffer().remove(service);
            service.decUserCounter();

            if (service.getUserCounter() == 0) {
                Node<Service> prev = servicesList.getPrev();
                prev.link = nodeService.link;
                service.setSubServices(null);
                System.out.println("Service removed.");
            }
        }

    }

    public ExtendedLinkList<Service> getServicesList() {
        return servicesList;
    }

    public boolean listOrders(String agencyName) throws NotInBound {

        int agencyInd = searchAgency(agencyName, agencies.getFirst());

        if (agencyInd == -1) {
            System.out.println("We don't have this agency :(");
            return false;
        } else {
            Agency agency = agencies.get(agencyInd);
            MaxHeap maxHeap = agency.getMaxHeapOfOrders();
            maxHeap.clear(agency, servicesList);
            return true;
        }
    }
}
