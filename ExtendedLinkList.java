package Project;

class ExtendedLinkList<E> {

    private int size = 0;
    private Node<Service> first = null, tail = null;
    private Node<Service> preServiceNode;

    private boolean IsEmpty() {
        return size == 0;
    }

    public boolean addService(Service service) {

        Node<Service> node1 = new Node<>();
        node1.data = service;
        node1.link = null;

        if (IsEmpty()) {
            size++;
            first = node1;
            tail = node1;
            System.out.println("Added :)");
            return true;
        }
        tail.link = node1;
        tail = node1;
        size++;
        System.out.println("Added :)");
        return true;
    }

    public Service getService(int index) {

        Node<Service> serviceNode = first;
        for (int i = 0; i < index; i++)
            serviceNode = serviceNode.link;

        return serviceNode.data;
    }

    public Node<Service> searchNodeService(String serviceName, Node<Service> node) {

        node.link = first;

        while (node.link != null) {
            Service service = node.link.data;

            if (service.getServiceName().equals(serviceName)) {

                preServiceNode = node;
                return node.link;
            } else {
                Node<Service> subNode = service.getSubServices();
                int subCounter = service.getNumberOfSubServices(), counter = 0;

                while (subNode.link != null && subCounter > 0 && counter <= subCounter) {
                    counter++;

                    if (subNode.link.data.getServiceName().equals(serviceName)) {

                        preServiceNode = subNode;
                        return subNode.link;
                    } else {
                        subNode = subNode.link;
                    }
                }
                node = node.link;
            }
        }
        return null;
    }

    public int getLength() {
        return size;
    }

    public Node<Service> getFirst() {
        return first;
    }

    @Override
    public String toString() {

        for (int i = 0; i < this.getLength(); i++) {
            System.out.println(this.getService(i).toString());
        }
        return "";
    }

    public Node<Service> getPrev() {
        return preServiceNode;
    }
}
