package Project;

import java.sql.Timestamp;

public class Order implements Comparable {

    private String orderServiceName, orderAgencyName, customerName;
    private int immediacyLevel;
    private Timestamp arrivalTime;

    public Order(String orderServiceName, String orderAgencyName, String customerName, int immediacyLevel, Timestamp arrivalTime) {
        this.orderServiceName = orderServiceName;
        this.orderAgencyName = orderAgencyName;
        this.customerName = customerName;
        this.immediacyLevel = immediacyLevel;
        this.arrivalTime = arrivalTime;
    }

    public String getOrderServiceName() {
        return orderServiceName;
    }

    @Override
    public int compareTo(Object order) {

        if (this.immediacyLevel > ((Order) order).immediacyLevel)
            return 1;
        else if (this.immediacyLevel == ((Order) order).immediacyLevel) {
            return this.arrivalTime.compareTo(((Order) order).arrivalTime);

        } else return -1;
    }

    @Override
    public String toString(){

        return "\nService name: " + orderServiceName
                + "\nAgency name: " + orderAgencyName
                + "\nCustomer name: " + customerName
                + "\nImmediacy Level: " + immediacyLevel
                + "\nArrival Time: " + arrivalTime;
    }
}