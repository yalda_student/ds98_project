package Project;

public class LinkedList<E extends Agency> {


    /**
     * this class is private because we have one node for iterating our list
     * sth like that..
     *
     * @param <E>
     */
    static class Node<E extends Agency> {

        Node<E> prev;
        E data;
        Node<E> next;

        public Node(Node<E> prev, E data, Node<E> next) {
            this.prev = prev;
            this.data = data;
            this.next = next;
        }

        private E getData() {
            return data;
        }

        private void setData(E element) {
            data = element;
        }
    }

    private int size = 0;
    private Node<E> first, tail;

    private boolean IsEmpty() {
        return getLength() == 0;
    }

    public boolean AddFirst(E Element) {

        try {
            if (IsEmpty()) {
                final Node<E> node = new Node<E>(null, Element, null);
                first = node;
                tail = node;
                size++;
                return true;
            }
            Node<E> node = new Node<E>(null, Element, first);
            first.prev = node;
            first = node;
            return true;
        } catch (java.lang.OutOfMemoryError memoryError) {
            memoryError.printStackTrace();
            return false;
        }
    }

    public boolean AddTail(E Element) {

        try {
            if (IsEmpty())
                return AddFirst(Element);
            else {
                Node<E> node = new Node<E>(tail, Element, null);
                tail.next = node;
                tail = node;
                size++;
                return true;
            }
        } catch (java.lang.OutOfMemoryError memoryError) {
            memoryError.printStackTrace();
            return false;
        }
    }

    public boolean Insert(E Element, int index) {

        try {
            if (index < 0 || index > size) {
                return false;
            } else if (index == 0)
                return AddFirst(Element);
            else if (index == size)
                return AddTail(Element);
            else {
                Node<E> current;
                if (index <= size / 2) {

                    current = first;
                    for (int i = 0; i < index; i++) {
                        current = current.next;
                    }
                    Node<E> node = new Node<E>(current, Element, current.next);
                    current.next.prev = node;
                    current.next = node;
                    size++;
                } else {
                    current = tail;
                    for (int i = size; i >= index - 1; i--) {
                        current = current.prev;
                    }
                    Node<E> node = new Node<E>(current.prev, Element, current);
                    current.next.prev = node;
                    current.next = node;
                }
                return true;
            }
        } catch (java.lang.OutOfMemoryError memoryError) {
            memoryError.printStackTrace();
            return false;
        }
    }

    public boolean RemoveFirst() {

        if (!IsEmpty()) {
            first = first.next;
            size--;
            first.prev = null;
            return true;
        } else return false;
    }

    public boolean RemoveTail() {

        if (!IsEmpty()) {
            tail = tail.prev;
            tail.next = null;
            size--;
            return true;
        } else return false;
    }

    public boolean RemoveAt(int index) {

        if (index < 0 || index >= size) {
            return false;
        } else if (index == 0)
            return RemoveFirst();
        else if (index == size - 1)
            return RemoveTail();
        else {
            Node<E> current = first;
            for (int i = 0; i <= index; i++) {
                current = current.next;
            }
            current.prev = current.next;
            size--;
            return true;
        }
    }

    public void clear() {
        size = 0;
    }

    public int getLength() {
        return size;
    }

    public Node<E> getFirst() {
        return first;
    }

    public E getTail() {
        return tail.data;
    }

    public E get(int index) throws NotInBound {

        if (index < 0 || index > size - 1)
            throw new NotInBound("invalid index");
        else if (index == 0)
            return first.data;
        else if (index == size - 1)
            return getTail();
        else {
            Node<E> temp = first;
            for (int i = 0; i <= index; i++) {
                temp = temp.next;
            }
            return temp.data;
        }
    }

}
